# Pokémon

This is an example about how to work Pokémon battle system.

## Getting started

You need Unity in 2021.3.11f1v for this project.

You can get a preview here.

[![Pokemon exervise video](https://img.youtube.com/vi/NXcRW2aWyB4/0.jpg)](https://www.youtube.com/watch?v=NXcRW2aWyB4)