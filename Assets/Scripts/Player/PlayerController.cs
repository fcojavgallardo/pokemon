using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    
    private bool isMoving = false;
    
    [Header("Params")]
    [SerializeField]
    [Tooltip("Set the speed of the player")]
    private float speed;
    private Vector2 inputPlayer;
    private Animator _animator;

    [Header("Layer")]
    [Tooltip("Layer where you can not pass")]
    public LayerMask solidObjectsLayer;
    [Tooltip("Layer where appear pok�mon")]
    public LayerMask pokemonLayer;

    public event Action OnPokemonEncountered;
    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }
    public void HandleUpdate()
    {
        if (!isMoving)
        {
            inputPlayer.x = Input.GetAxisRaw("Horizontal");
            inputPlayer.y = Input.GetAxisRaw("Vertical");

            if(inputPlayer.x != 0)
            {
                inputPlayer.y = 0;
            }

            if (inputPlayer != Vector2.zero)
            {
                _animator.SetFloat("MoveX", inputPlayer.x);
                _animator.SetFloat("MoveY", inputPlayer.y);

                var targetPosition = transform.position;
                targetPosition.x += inputPlayer.x;
                targetPosition.y += inputPlayer.y;

                if(IsPossibleWalk(targetPosition))
                {
                    StartCoroutine(MoveTowards(targetPosition));
                }
            }
        }
    }

    private void LateUpdate()
    {
        _animator.SetBool("IsMoving", isMoving);
    }

    /// <summary>
    /// Releases a Coroutine to move the player until the destination
    /// </summary>
    /// <param name="destination">The destionation Vector3 point</param>
    IEnumerator MoveTowards(Vector3 destination)
    {
        isMoving = true;
        while(Vector3.Distance(transform.position, destination) > Mathf.Epsilon)
        {
            transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);

            yield return null;
        }
        transform.position = destination;
        isMoving = false;

        CheckIsABattle();
    }

    /// <summary>
    /// This will check if you can make the next step.
    /// create a circle in the targetPosition and check that
    /// there is not another collision there.
    /// </summary>
    /// <param name="target">The place where you want to go</param>
    /// <returns>true if you can go, and false if not.</returns>
    private bool IsPossibleWalk(Vector3 targetPosition)
    {
        return !Physics2D.OverlapCircle(targetPosition, 0.2f, solidObjectsLayer);
    }

    private void CheckIsABattle()
    {
        if (Physics2D.OverlapCircle(transform.position, 0.2f, pokemonLayer))
        {
            if(UnityEngine.Random.Range(0, 100) < 10) 
            {
                OnPokemonEncountered();
            }
        }
    }
}
