using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PartyMemberHUD : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Text nameText;
    [SerializeField] Text lvlText;
    [SerializeField] Text typeText;
    [SerializeField] Text HpText;

    [SerializeField] HealthBar healthBar;
    [SerializeField] Image pokemonImage;

    Pokemon _pokemon;

    [SerializeField] Color selectedColor = Color.blue;

    public void SetPokemonData(Pokemon pokemon)
    {
        _pokemon = pokemon;

        nameText.text = pokemon.Base.Name;
        lvlText.text = $"Lv {pokemon.Level}";
        typeText.text = pokemon.Base.Type1.ToString().ToUpper();
        HpText.text = $"{pokemon.HP}/{pokemon.MaxHP}";

        healthBar.SetHP((float)pokemon.HP/pokemon.MaxHP);
        pokemonImage.sprite = pokemon.Base.FrontSprite;

        GetComponent<Image>().color = TypeColor.GetColorFromType(pokemon.Base.Type1);
    }

    public void SetSelectedPokemon(bool selected)
    {
        if(selected)
        {
            nameText.color = selectedColor;
        } else
        {
            nameText.color = Color.black;
        }
    }
}