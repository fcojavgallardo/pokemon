using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SelectionMovementUI : MonoBehaviour
{
    [SerializeField] Text[] movementTexts;
    [SerializeField] Color selectedColor = Color.blue;

    int currentSelectedMovement = 0;

    private void Awake()
    {
        movementTexts = GetComponentsInChildren<Text>(true);
    }
    public void SetMovements(List<MoveBase> pokemonMoves, MoveBase newMove)
    {
        currentSelectedMovement = 0;

        for(int i = 0; i < pokemonMoves.Count; i++)
        {
            movementTexts[i].text = pokemonMoves[i].name;
        }

        movementTexts[pokemonMoves.Count].text = newMove.name;
    }

    public void HandleForgetMoveSelection(System.Action<int> onSelected)
    {
        if (Input.GetAxisRaw("Vertical")!= 0)
        {
            int direction = Mathf.FloorToInt(Input.GetAxisRaw("Vertical"));
            currentSelectedMovement -= direction;
            onSelected?.Invoke(-1);
        }
        
        currentSelectedMovement = Mathf.Clamp(currentSelectedMovement, 0, PokemonBase.NUMBER_OF_LEARNABLES_MOVES);
        UpdateColorForgetMoveSelection(currentSelectedMovement);

        if (Input.GetAxisRaw("Submit") != 0)
        {
            onSelected?.Invoke(currentSelectedMovement);
        }
    }

    public void UpdateColorForgetMoveSelection(int selectedMove)
    {
        for (int i = 0; i <= PokemonBase.NUMBER_OF_LEARNABLES_MOVES; i++)
        {
            movementTexts[i].color = i == selectedMove ? selectedColor : Color.black;
        }
    }
}
