using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;
using System.Linq;

public enum BattleState
{
    StartBattle,
    ActionSelection,
    MovementSelection,
    PerformMove,
    Busy,
    PartySelectScreen,
    ItemSelecteScreen,
    ForgetMovement,
    LoseTurn,
    FinishBattle
}


public enum BattleType
{
    WildPokemon,
    Trainer,
    Leader
}

public class BattleManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] BatleUnit playerUnit;
    [SerializeField] BatleUnit enemyUnit;

    [SerializeField] BattleDialogBox battleDialogBox;

    [SerializeField] PartyHUD partyHud;
    [SerializeField] SelectionMovementUI selectionMovementUi;

    [SerializeField] GameObject pokeball;

    public event UnityAction<bool> OnBattleFinish;

    [SerializeField] float timeBetweenClicks = 0.5f;
    float timeSinceLastClick;

    int currentSelectedAction;
    int currentSelectedMovement;
    int currentSelectedPokemon;

    int escapeAttempts;

    public BattleState state;
    public BattleType battleType;

    PokemonParty playerParty;
    Pokemon wildPokemon;

    MoveBase moveToLearn;

    public AudioClip attackClip;
    public AudioClip damageClip;
    public AudioClip levelUpClip;
    public AudioClip runBattleClip;
    public AudioClip launchPokeball;
    public AudioClip shakeSound;

    void PlayerActionSelection()
    {
        currentSelectedAction = 0;
        state = BattleState.ActionSelection;
        StartCoroutine(battleDialogBox.SetDialog("Selecciona una acci�n"));

        battleDialogBox.ToggleDialogText(true);
        battleDialogBox.ToggleActions(true);
        battleDialogBox.ToggleMovements(false);
        
        battleDialogBox.SelectAction(currentSelectedAction);
    }

    void PlayerMovementSelection()
    {
        state = BattleState.MovementSelection;

        battleDialogBox.ToggleDialogText(false);
        battleDialogBox.ToggleActions(false);
        battleDialogBox.ToggleMovements(true);

        currentSelectedMovement = 0;
        battleDialogBox.SelectMovement(currentSelectedMovement, playerUnit.Pokemon.Moves[currentSelectedMovement]);
    }

    void OpenPartySelectionScreen()
    {
        state = BattleState.PartySelectScreen;
        partyHud.SetPartyData(playerParty.Pokemons);
        partyHud.gameObject.SetActive(true);
        currentSelectedPokemon = playerParty.GetPositionFromPokemon(playerUnit.Pokemon);
        
        partyHud.UpdateSelectedPokemon(currentSelectedPokemon);
    }

    void OpenInventoryScreen()
    {
        battleDialogBox.ToggleActions(false);
        StartCoroutine(ThrowPokeball());
    }

    void HandlePlayerActionSelection()
    {
        if(Input.GetAxisRaw("Vertical") !=0)
        {
            timeSinceLastClick = 0;
            currentSelectedAction = (currentSelectedAction + 2) % PokemonBase.NUMBER_OF_LEARNABLES_MOVES;
            battleDialogBox.SelectAction(currentSelectedAction);
        } else if (Input.GetAxisRaw("Horizontal") != 0)
        {
            timeSinceLastClick = 0;
            currentSelectedAction = (currentSelectedAction + 1) % 2 
                + 2 * Mathf.FloorToInt(currentSelectedAction / 2);
            battleDialogBox.SelectAction(currentSelectedAction);
        }

        if (Input.GetAxisRaw("Submit") != 0)
        {
            timeSinceLastClick = 0;
            // Lucha
            if (currentSelectedAction == 0)
            {
                PlayerMovementSelection();
            } 
            // Pok�mon
            else if (currentSelectedAction == 1)
            {
                OpenPartySelectionScreen();
            }
            // Mochila
            else if (currentSelectedAction == 2)
            {
                OpenInventoryScreen();
            }
            // Huir
            else if (currentSelectedAction == 3)
            {
                StartCoroutine(TryToEscapeFormBattle());
            }
        }
    }

    void HandlePartySelection()
    {
        if (Input.GetAxisRaw("Vertical") != 0)
        {
            timeSinceLastClick = 0;
            currentSelectedPokemon -= (int)Input.GetAxisRaw("Vertical") * 2;
        }
        else if (Input.GetAxisRaw("Horizontal") != 0)
        {
            timeSinceLastClick = 0;
            currentSelectedPokemon += (int)Input.GetAxisRaw("Horizontal");
        }

        currentSelectedPokemon = Mathf.Clamp(currentSelectedPokemon, 0, playerParty.Pokemons.Count - 1);
        partyHud.UpdateSelectedPokemon(currentSelectedPokemon);

        if (Input.GetAxisRaw("Submit") != 0)
        {
            timeSinceLastClick = 0;
            var selectedPokemon = playerParty.Pokemons[currentSelectedPokemon];
            if(selectedPokemon.HP <= 0)
            {
                partyHud.SetMessage("No puedes enviar un pok�mon debilitado");
                return;
            } else if (selectedPokemon == playerUnit.Pokemon)
            {
                partyHud.SetMessage($"{playerUnit.Pokemon.Base.Name} ya est� luchando");
                return;
            }

            partyHud.gameObject.SetActive(false);
            state = BattleState.Busy;
            StartCoroutine(SwitchPokemon(selectedPokemon));
        }

        if (Input.GetAxisRaw("Cancel") != 0)
        {
            partyHud.gameObject.SetActive(false);
            PlayerActionSelection();
        }
    }

    void HandlePlayerMovementSelection()
    {
        if (Input.GetAxisRaw("Vertical") != 0)
        {
            timeSinceLastClick = 0;
            var oldSelectedMovement = currentSelectedMovement;
            currentSelectedMovement = (currentSelectedMovement + 2) % 4;
            if(currentSelectedMovement >= playerUnit.Pokemon.Moves.Count)
            {
                currentSelectedMovement = oldSelectedMovement;
            }
            battleDialogBox.SelectMovement(currentSelectedMovement, playerUnit.Pokemon.Moves[currentSelectedMovement]);
        }
        else if (Input.GetAxisRaw("Horizontal") != 0)
        {
            timeSinceLastClick = 0;
            var oldSelectedMovement = currentSelectedMovement;

            currentSelectedMovement = (currentSelectedMovement + 1) % 2 +
                2 * Mathf.FloorToInt(currentSelectedMovement / 2);

            if (currentSelectedMovement >= playerUnit.Pokemon.Moves.Count)
            {
                currentSelectedMovement = oldSelectedMovement;
            }

            battleDialogBox.SelectMovement(currentSelectedMovement, playerUnit.Pokemon.Moves[currentSelectedMovement]);
        }

        if (Input.GetAxisRaw("Submit") != 0)
        {
            timeSinceLastClick = 0;
            battleDialogBox.ToggleMovements(false);
            battleDialogBox.ToggleActions(false);
            battleDialogBox.ToggleDialogText(true);
            StartCoroutine(PerformPlayerMovement());
        }

        if (Input.GetAxisRaw("Cancel") != 0)
        {
            PlayerActionSelection();
        }

    }

    void CheckForBattleFinish(BatleUnit faintedUnit)
    {
        if (faintedUnit.IsPlayer)
        {
            var nextPokemon = playerParty.GetFirstNonFaintedPokemon();

            if (nextPokemon != null)
            {
                OpenPartySelectionScreen();
            }
            else
            {
                BattleFinish(false);
            }
        }
        else
        {
            BattleFinish(true);
        }
    }

    void BattleFinish(bool playerHasWon)
    {
        state = BattleState.FinishBattle;
        OnBattleFinish(playerHasWon);
    }

    int TryToCatchPokemon(Pokemon pokemon)
    {
        float bonusPokeball = 1; // TODO: clase pokeball con su multiplicador
        float bonusStat = 1; // TODO: stats para chequear condici�n de modificaci�n
        float a = (3 * pokemon.MaxHP - 2 * pokemon.HP) * pokemon.Base.CatchRate *
            bonusPokeball * bonusStat / (3 * pokemon.MaxHP);

        if (a >= 255)
        {
            return 4;
        }

        float b = 1048560 / Mathf.Sqrt(Mathf.Sqrt(16711680 / a));

        int shakeCount = 0;

        while (shakeCount < 4)
        {
            if (Random.Range(0, 65535) >= b)
            {
                break;
            }
            else
            {
                shakeCount++;
            }
        }

        return shakeCount;
    }

    IEnumerator showDamageDescription(Pokemon.DamageDescription description)
    {
        if (description.Critical > 1f)
        {
            yield return battleDialogBox.SetDialog("�Un golpe cr�tico!");
        }

        if (description.Type > 1)
        {
            yield return battleDialogBox.SetDialog("�Es super efectivo!");
        }
        else if (description.Type < 1)
        {
            yield return battleDialogBox.SetDialog("Es poco eficaz...");
        }
    }

    IEnumerator SwitchPokemon(Pokemon newPokemon)
    {
        if (playerUnit.Pokemon.HP > 0)
        {
            yield return battleDialogBox.SetDialog($"vuelve {playerUnit.Pokemon.Base.Name}");
            playerUnit.PlayFaintAnimation();
            yield return new WaitForSeconds(1.5f);
        }

        playerUnit.SetupPokemon(newPokemon);
        battleDialogBox.SetPokemonMovements(newPokemon.Moves);
        yield return battleDialogBox.SetDialog($"adelante {newPokemon.Base.Name}");

        yield return new WaitForSeconds(.5f);
        StartCoroutine(PerformEnemyMovement());
    }

    IEnumerator PerformPlayerMovement()
    {
        state = BattleState.PerformMove;

        Move move = playerUnit.Pokemon.Moves[currentSelectedMovement];

        if(move.Pp <= 0)
        {
            PlayerMovementSelection();
            yield break;
        }

        yield return RunMovement(playerUnit, enemyUnit, move);

        if (state == BattleState.PerformMove)
        {
            StartCoroutine(PerformEnemyMovement());
        }
        
    }

    IEnumerator PerformEnemyMovement()
    {
        state = BattleState.PerformMove;
        Move move = enemyUnit.Pokemon.RandomMove();

        yield return RunMovement(enemyUnit, playerUnit, move);

        if (state == BattleState.PerformMove)
        {
            PlayerActionSelection();
        }
            
    }

    IEnumerator RunMovement(BatleUnit attacker, BatleUnit target, Move move)
    {
        move.Pp--;
        yield return battleDialogBox.SetDialog($"{attacker.Pokemon.Base.Name} us� {move.Base.name}");

        var oldHpValue = target.Pokemon.HP;

        attacker.PlayAttackAnimation();
        SoundManager.SharedInstance.PlaySound(attackClip);
        yield return new WaitForSeconds(1f);
        target.PlayReceiveAttackAnimation();
        SoundManager.SharedInstance.PlaySound(damageClip);

        var damageDescription = target.Pokemon.ReceiveDamage(attacker.Pokemon, move);
        yield return target.Hud.UpdatePokemonData(oldHpValue);
        yield return showDamageDescription(damageDescription);

        if (damageDescription.Fainted)
        {
            yield return HandlePokemonFainted(target);
        }

    }

    IEnumerator ThrowPokeball()
    {
        state = BattleState.Busy;

        if(battleType != BattleType.WildPokemon)
        {
            yield return battleDialogBox.SetDialog("No puede robar los pok�mon de otros entrenadores");
            state = BattleState.LoseTurn;
            yield break;
        }

        yield return battleDialogBox.SetDialog("�Has lanzado una pokeball!");

        GameObject pokeballInstance = Instantiate(
            pokeball, 
            playerUnit.transform.position - new Vector3(1, 2),
            Quaternion.identity);

        SoundManager.SharedInstance.PlaySound(launchPokeball);
        var pokeballSprite = pokeballInstance.GetComponent<SpriteRenderer>();

        yield return pokeballSprite.transform.DOLocalJump(
            enemyUnit.transform.position + new Vector3(2, 2),
            2f,
            1,
            1f).WaitForCompletion();

        yield return enemyUnit.PlayCaptureAnimation();

        yield return pokeballSprite.transform.DOLocalMoveY(
            enemyUnit.transform.position.y -3, 0.5f
            ).WaitForCompletion();

        var numberOfShakes = TryToCatchPokemon(enemyUnit.Pokemon);

        for(int i = 0; i < Mathf.Min(numberOfShakes, 3); i++)
        {
            yield return new WaitForSeconds(0.5f);
            SoundManager.SharedInstance.PlaySound(shakeSound);
            yield return pokeballSprite.transform.DOPunchRotation(new Vector3(0, 0, 15f), 1f).WaitForCompletion();
        }

        if(numberOfShakes == 4)
        {
            yield return battleDialogBox.SetDialog($"�{enemyUnit.Pokemon.Base.Name} capturado!");
            yield return pokeballSprite.DOFade(0, 1.5f).WaitForCompletion();

            if (playerParty.AddPokemonToParty(enemyUnit.Pokemon))
            {
                yield return battleDialogBox.SetDialog($"�{enemyUnit.Pokemon.Base.Name} a�adido al equipo Pok�mon!");
            } else
            {
                yield return battleDialogBox.SetDialog($"�oh, no! No te caben m�s Pok�mon en el equipo");
            }

            Destroy(pokeballInstance);
            BattleFinish(true);
        }else
        {
            yield return new WaitForSeconds(0.5f);
            pokeballSprite.DOFade(0, 0.2f);
            yield return enemyUnit.PlayBreakOutAnimation();

            if(numberOfShakes <2)
            {
                yield return battleDialogBox.SetDialog($"�{enemyUnit.Pokemon.Base.Name} ha escapado!");

            } else
            {
                yield return battleDialogBox.SetDialog($"�Te ha faltado poco!");
            }

            Destroy(pokeballInstance);
            state = BattleState.LoseTurn;

        }

    }

    IEnumerator TryToEscapeFormBattle()
    {
        state = BattleState.Busy;

        if(battleType != BattleType.WildPokemon)
        {
            yield return battleDialogBox.SetDialog("No puedes huir de combates contra entrenadores Pokemon");
            state = BattleState.LoseTurn;
        }

        escapeAttempts++;

        int playerSpeed = playerUnit.Pokemon.Speed;
        int enemySpeed = enemyUnit.Pokemon.Speed;

        if(playerSpeed >= enemySpeed)
        {
            SoundManager.SharedInstance.PlaySound(runBattleClip);
            yield return battleDialogBox.SetDialog("Has escapado con �xito");
            yield return new WaitForSeconds(1.0f);
            OnBattleFinish(true);
        }
        else
        {
            float oddEscape = (Mathf.FloorToInt(playerSpeed * 128 / enemySpeed) + 30 * escapeAttempts) % 256;
            if(Random.Range(0,256) < oddEscape)
            {
                yield return battleDialogBox.SetDialog("Has escapado con �xito");
                yield return new WaitForSeconds(1.0f);
                OnBattleFinish(true);
            } else
            {
                yield return battleDialogBox.SetDialog("No puedes huir");
                escapeAttempts++;
                state = BattleState.LoseTurn;
            }
        }
    }

    IEnumerator HandlePokemonFainted(BatleUnit faintedUnit)
    {
        yield return battleDialogBox.SetDialog($"{faintedUnit.Pokemon.Base.Name} se ha debilitado");
        faintedUnit.PlayFaintAnimation();
        yield return new WaitForSeconds(1.5f);

        if (!faintedUnit.IsPlayer)
        {
            // EXP ++
            int expBase = faintedUnit.Pokemon.Base.ExpBase;
            int level = faintedUnit.Pokemon.Level;

            float multiplier = (battleType == BattleType.WildPokemon ? 1 : 1.5f);

            int wonExp = Mathf.FloorToInt(expBase * level * multiplier / 7);
            playerUnit.Pokemon.Experience += wonExp;

            yield return battleDialogBox.SetDialog($"{playerUnit.Pokemon.Base.name} ha ganado {wonExp} puntos de experiencia");
            yield return playerUnit.Hud.SetExpSmooth();
            yield return new WaitForSeconds(1.5f);
            // Check New Level
            while (playerUnit.Pokemon.NeedsToLevelUp())
            {
                SoundManager.SharedInstance.PlaySound(levelUpClip);
                playerUnit.Hud.SetLevelText();
                yield return playerUnit.Hud.UpdatePokemonData(playerUnit.Pokemon.HP);
                yield return battleDialogBox.SetDialog($"{playerUnit.Pokemon.Base.Name} ha subido de nivel");
                //Intentar aprender nuevo movimiento
                var newLearnableMove = playerUnit.Pokemon.GetLearnableMoveAtCurrentLevel();
                if(newLearnableMove != null)
                {
                    if(playerUnit.Pokemon.Moves.Count < PokemonBase.NUMBER_OF_LEARNABLES_MOVES)
                    {
                        //podemos aprender el movimiento
                        playerUnit.Pokemon.LearnMove(newLearnableMove);
                        yield return battleDialogBox.SetDialog($"{playerUnit.Pokemon.Base.name} ha aprendido {newLearnableMove.Move.name}");
                        battleDialogBox.SetPokemonMovements(playerUnit.Pokemon.Moves);
                    } else
                    {
                        //Olvidar uno de los movimientos
                        yield return battleDialogBox.SetDialog($"{playerUnit.Pokemon.Base.Name} intenta aprender {newLearnableMove.Move.name}");
                        yield return battleDialogBox.SetDialog($"Pero no puede aprender m�s de {PokemonBase.NUMBER_OF_LEARNABLES_MOVES} movimientos");
                        yield return ChooseMovementToForget(playerUnit.Pokemon, newLearnableMove.Move);
                        yield return new WaitUntil(() => state != BattleState.ForgetMovement);
                    }
                }
                yield return playerUnit.Hud.SetExpSmooth(true);
            }
        }
        CheckForBattleFinish(faintedUnit);
    }

    IEnumerator ChooseMovementToForget(Pokemon learner, MoveBase newMove)
    {
        state = BattleState.Busy;
        yield return battleDialogBox.SetDialog("Selecciona el movimiento que quieres olvidar");
        selectionMovementUi.gameObject.SetActive(true);
        selectionMovementUi.SetMovements(
            learner.Moves.Select(move => move.Base).ToList(), 
            newMove);
        moveToLearn = newMove;
        state = BattleState.ForgetMovement;
    }

    IEnumerator ForgetOldMove(int moveIndex)
    {
        selectionMovementUi.gameObject.SetActive(false);

        if (moveIndex == PokemonBase.NUMBER_OF_LEARNABLES_MOVES)
        {
            yield return battleDialogBox.SetDialog($"{playerUnit.Pokemon.Base.name} no aprendi� {moveToLearn.name}");
        }
        else
        {
            var selectedMove = playerUnit.Pokemon.Moves[moveIndex].Base;
            yield return battleDialogBox.SetDialog($"{playerUnit.Pokemon.Base.name} olvid� {selectedMove.name} y aprendi� {moveToLearn.name}");
            playerUnit.Pokemon.Moves[moveIndex] = new Move(moveToLearn);
        }

        moveToLearn = null;
        if (enemyUnit.Pokemon.HP <= 0)
        {
            //TODO: Revisar cuando haya entrenadores
            state = BattleState.FinishBattle;
        }
    }

    public IEnumerator SetupBattle()
    {
        state = BattleState.StartBattle;

        playerUnit.SetupPokemon(playerParty.GetFirstNonFaintedPokemon());

        battleDialogBox.SetPokemonMovements(playerUnit.Pokemon.Moves);

        enemyUnit.SetupPokemon(wildPokemon);

        partyHud.InitPartyHUD();

        yield return battleDialogBox.SetDialog($"Un {enemyUnit.Pokemon.Base.Name} salvaje apareci�.");

        if (enemyUnit.Pokemon.Speed >= playerUnit.Pokemon.Speed)
        {
            battleDialogBox.ToggleActions(false);
            battleDialogBox.ToggleMovements(false);
            battleDialogBox.ToggleDialogText(true);

            yield return battleDialogBox.SetDialog("El enemigo ataca primero");
            yield return PerformEnemyMovement();
        }
        else
        {
            PlayerActionSelection();
        }
    }

    public void HandleStartTrainerBattle(PokemonParty playerParty, PokemonParty trainerParty, bool isLeader)
    {
        battleType =  isLeader ? BattleType.Trainer : BattleType.Leader;
    }
    public void HandleStartBattle(PokemonParty playerParty, Pokemon wildPokemon)
    {
        battleType = BattleType.WildPokemon;
        escapeAttempts = 0;
        this.playerParty = playerParty;
        this.wildPokemon = wildPokemon;
        StartCoroutine(SetupBattle());
    }
    public void HandleUpdate()
    {
        timeSinceLastClick += Time.deltaTime;

        if(timeSinceLastClick < timeBetweenClicks || battleDialogBox.isWritting)
        {
            return;
        }

        if(state == BattleState.ActionSelection)
        {
            HandlePlayerActionSelection();
        } else if(state == BattleState.MovementSelection)
        {
            HandlePlayerMovementSelection();
        } else if(state == BattleState.PartySelectScreen)
        {
            HandlePartySelection();
        } else if(state == BattleState.LoseTurn)
        {
            StartCoroutine(PerformEnemyMovement());
        } else if (state == BattleState.ForgetMovement)
        {
            selectionMovementUi.HandleForgetMoveSelection((moveIndex) =>
            {
                if(moveIndex < 0)
                {
                    timeSinceLastClick = 0;
                    return;
                }

                StartCoroutine(ForgetOldMove(moveIndex));
            });
        }
    }
}
