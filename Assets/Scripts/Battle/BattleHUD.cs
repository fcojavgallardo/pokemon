using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BattleHUD : MonoBehaviour
{
    // Start is called before the first frame update
    public Text pokemonName;
    public Text pokemonLevel;
    public HealthBar healthbar;
    public Text pokemonHealth;
    public GameObject expbar;

    Pokemon _pokemon;

    public void SetPokemonData(Pokemon pokemon)
    {
        _pokemon = pokemon;
        pokemonName.text = pokemon.Base.Name;
        SetLevelText();
        healthbar.SetHP((float)_pokemon.HP / _pokemon.MaxHP);
        SetExp();
        StartCoroutine(UpdatePokemonData(pokemon.HP));
    }

    public IEnumerator UpdatePokemonData(int oldHPVal)
    {
        StartCoroutine(healthbar.SetSmoothHp((float)_pokemon.HP / _pokemon.MaxHP));
        StartCoroutine(DecreaseHealthPoints(oldHPVal));
        yield return null;
    }

    public IEnumerator DecreaseHealthPoints(int oldHpVal)
    {
        while(oldHpVal > _pokemon.HP)
        {
            oldHpVal--;
            pokemonHealth.text = $"{oldHpVal}/{_pokemon.MaxHP}";
            yield return new WaitForSeconds(0.1f);
        }
        pokemonHealth.text = $"{_pokemon.HP}/{_pokemon.MaxHP}";

    }

    public void SetExp()
    {
        if (expbar == null)
        {
            return;
        }

        expbar.transform.localScale = new Vector3(NormalizedExp(), 1, 1);
    }

    public void SetLevelText()
    {
        pokemonLevel.text = $"Lv {_pokemon.Level}";
    }

    public IEnumerator SetExpSmooth(bool needsToResetBar = false)
    {
        if (expbar == null)
        {
            yield break;
        }

        if(needsToResetBar)
        {
            expbar.transform.localScale = new Vector3(0, 1, 1);
        }

        yield return expbar.transform.DOScaleX(NormalizedExp(), 1.5f);
    }

    float NormalizedExp()
    {
        float currentLevelExp = _pokemon.Base.GetNecessaryExpForLevel(_pokemon.Level);
        float nextLevelExp = _pokemon.Base.GetNecessaryExpForLevel(_pokemon.Level +1);

        float normalizedExp = (_pokemon.Experience - currentLevelExp) / (nextLevelExp - currentLevelExp);

        return Mathf.Clamp01(normalizedExp);
    }
}
