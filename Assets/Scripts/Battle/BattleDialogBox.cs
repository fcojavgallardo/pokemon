using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleDialogBox : MonoBehaviour
{
    [SerializeField] Text dialogText;
    public float charactersPerSecond = 10.0f;
    [SerializeField] GameObject actionSelect;
    [SerializeField] GameObject movementSelect;
    [SerializeField] GameObject movementDescription;

    [Header ("Acciones y Movimientos")]
    [SerializeField] List<Text> actionTexts;
    [SerializeField] List<Text> movementTexts;

    [Header("Datos del ataque")]
    [SerializeField] Text ppText;
    [SerializeField] Text typeText;

    public bool isWritting = false;

    Color selectedColor;

    [SerializeField] float timeToWaitAfterText = 1.0f;

    private void Start()
    {
        selectedColor = Color.blue;
    }
    public IEnumerator SetDialog(string message)
    {
        isWritting = true;

        dialogText.text = "";
        foreach (char character in message) {
            dialogText.text += character;
            yield return new WaitForSeconds(1 / charactersPerSecond);
        }

        yield return new WaitForSeconds(timeToWaitAfterText);
        isWritting = false;
    }

    public void ToggleDialogText(bool activated)
    {
        dialogText.enabled = activated;
    }

    public void ToggleActions(bool activated)
    {
        actionSelect.SetActive(activated);
    }

    public void ToggleMovements(bool activated)
    {
        movementSelect.SetActive(activated);
        movementDescription.SetActive(activated);
    }

    public void SelectAction(int selectedAction)
    {
        for (int i = 0; i < actionTexts.Count; i++)
        {
           actionTexts[i].color = i == selectedAction ? selectedColor : Color.black;
        }
    }

    public void SelectMovement (int selectedMovement, Move move)
    {
        for (int i = 0; i < movementTexts.Count; i++)
        {
            movementTexts[i].color = i == selectedMovement ? selectedColor : Color.black;
        }

        ppText.text = $"PP {move.Pp}/{move.Base.Pp}";
        typeText.text = move.Base.Type.ToString().ToUpper();

        ppText.color = move.Pp <= 0  ? Color.red : Color.black;
    }

    public void SetPokemonMovements(List<Move> moves)
    {
        for (int i = 0; i < movementTexts.Count; i++)
        {
            if(i < moves.Count)
            {
                movementTexts[i].text = moves[i].Base.name;
            }
            else
            {
                movementTexts[i].text = "--";
            }
        }
    }

}
