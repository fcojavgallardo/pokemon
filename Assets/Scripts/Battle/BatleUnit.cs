using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(Image))]
public class BatleUnit : MonoBehaviour
{
    public PokemonBase pokemonBase;
    public int pokemonLevel;
    [SerializeField] bool isPlayer;
    [SerializeField] BattleHUD hud;

    public bool IsPlayer => isPlayer;
    public BattleHUD Hud => hud;

    Image pokemonImage;
    Vector3 initialPosition;
    Color initialColor;

    [SerializeField] float startTimeAnimation = 1f;
    [SerializeField] float attackTimeAnimation = 0.3f;
    [SerializeField] float dieTimeAnimation = 1f;
    [SerializeField] float hitTimeAnimation = 0.15f;

    [SerializeField] float captureAnimation = 0.5f;

    public AudioClip CapturingPokemon;

    private void Awake()
    {
        pokemonImage = GetComponent<Image>();
        initialColor = pokemonImage.color;
        initialPosition = pokemonImage.transform.localPosition;
    }

    public Pokemon Pokemon { get; set; }

    public void SetupPokemon(Pokemon pokemon)
    {
        Pokemon = pokemon;
        pokemonImage.sprite = isPlayer ? 
            Pokemon.Base.BackSprite : Pokemon.Base.FrontSprite;
        pokemonImage.color = initialColor;

        hud.SetPokemonData(pokemon);
        transform.localScale = new Vector3(1, 1, 1);

        PlayStartAnimation();
    }

    public void PlayStartAnimation()
    {
        pokemonImage.transform.localPosition = new Vector3(
            initialPosition.x + (isPlayer ? -400 : 400),
            initialPosition.y);

        pokemonImage.transform.DOLocalMoveX(initialPosition.x, startTimeAnimation);
    }

    public void PlayAttackAnimation()
    {
        var sequence = DOTween.Sequence();

        sequence.Append(pokemonImage.transform.DOLocalMoveX(
                initialPosition.x + (isPlayer ? 50 : -50),
                attackTimeAnimation
            ));
        sequence.Append(pokemonImage.transform.DOLocalMoveX(
                initialPosition.x,
                attackTimeAnimation
            ));
    }

    public void PlayReceiveAttackAnimation()
    {
        var sequence = DOTween.Sequence();

        sequence.Append(pokemonImage.DOColor(Color.gray, hitTimeAnimation));
        sequence.Append(pokemonImage.DOColor(initialColor, hitTimeAnimation));
        sequence.Append(pokemonImage.DOColor(Color.gray, hitTimeAnimation));
        sequence.Append(pokemonImage.DOColor(initialColor, hitTimeAnimation));
    }

    public void PlayFaintAnimation()
    {
        var sequence = DOTween.Sequence();

        sequence.Append(pokemonImage.transform.DOLocalMoveY(initialPosition.y - 200, dieTimeAnimation));
        sequence.Join(pokemonImage.DOFade(0f, dieTimeAnimation));
    }

    public IEnumerator PlayCaptureAnimation()
    {
        var sequence = DOTween.Sequence();

        SoundManager.SharedInstance.PlaySound(CapturingPokemon);
        sequence.Append(pokemonImage.DOFade(0, captureAnimation));
        sequence.Join(transform.DOScale(new Vector3(0.25f, 0.25f, 1f), captureAnimation));
        sequence.Join(transform.DOLocalMoveY(initialPosition.y + 120f, captureAnimation));
        sequence.Join(transform.DOLocalMoveX(initialPosition.x + 80f, captureAnimation));
        sequence.Join(pokemonImage.DOColor(Color.red, hitTimeAnimation));
        yield return sequence.WaitForCompletion();
    }

    public IEnumerator PlayBreakOutAnimation()
    {
        var sequence = DOTween.Sequence();

        sequence.Append(pokemonImage.DOFade(1, captureAnimation));
        sequence.Join(transform.DOScale(new Vector3(1f, 1f, 1f), captureAnimation));
        sequence.Join(transform.DOLocalMoveY(initialPosition.y, captureAnimation));
        sequence.Join(transform.DOLocalMoveX(initialPosition.x, captureAnimation));
        sequence.Join(pokemonImage.DOColor(initialColor, hitTimeAnimation));
        yield return sequence.WaitForCompletion();
    }
}
