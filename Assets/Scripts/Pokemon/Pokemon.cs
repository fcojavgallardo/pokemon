using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;
using System.Linq;

[Serializable]
public class Pokemon 
{
    [SerializeField] PokemonBase _base;
    [SerializeField] int _level;
    private int _experience;
    private int _currentHp;
    private List<Move> _moves;

    /// <summary>
    /// Devuelve los conceptos bases del pok�mon, como el nombre, descripci�n...
    /// </summary>
    public PokemonBase Base => _base;
    /// <summary>
    /// Devuelve el nivel del pok�mon
    /// </summary>
    public int Level => _level;

    /// <summary>
    /// HP del pok�mon
    /// </summary>
    public int HP
    {
        get => _currentHp;
        set {
            _currentHp = value;
            _currentHp = Mathf.FloorToInt(Mathf.Clamp(_currentHp, 0, MaxHP));
        }
    }

    /// <summary>
    /// Vida m�xima del pok�mon
    /// </summary>
    public int MaxHP => Mathf.FloorToInt((_base.MaxHP * _level) / 20.0f) + 10;
    /// <summary>
    /// Ataque del pok�mon
    /// </summary>
    public int Attack => Mathf.FloorToInt((_base.Attack*_level)/100.0f)+2;
    /// <summary>
    /// Defensa del pok�mon
    /// </summary>
    public int Defense => Mathf.FloorToInt((_base.Defense * _level) / 100.0f) + 2;
    /// <summary>
    /// Ataque especial del pok�mon
    /// </summary>
    public int SpAttack => Mathf.FloorToInt((_base.SpAttack * _level) / 100.0f) + 2;
    /// <summary>
    /// Defensa especial del pok�mon
    /// </summary>
    public int SpDefense => Mathf.FloorToInt((_base.SpDefense * _level) / 100.0f) + 2;
    /// <summary>
    /// Velocidad del pok�mon
    /// </summary>
    public int Speed => Mathf.FloorToInt((_base.Speed * _level) / 100.0f) + 2;

    public int Experience
    {
        get => _experience;
        set => _experience = value;
    }

    public List<Move> Moves
    {
        get => _moves;
        set => _moves = value;
    }

    public Pokemon(PokemonBase pBase, int pLevel)
    {
        _base = pBase;
        _level = pLevel;
        InitPokemon();
    }
    public void InitPokemon()
    {
        _currentHp = MaxHP;
        _experience = Base.GetNecessaryExpForLevel(_level);
        _moves = new List<Move>();

        /*
         *  Comprueba todos los movimientos que puede aprender el pok�mon.
         *  Si el pok�mon se encuentra en un nivel superior al ataque que ya
         *  ha aprendido, lo a�ade, y si ha aprendido m�s de 4, se terminar el
         *  bucle
         *  
         *  TODO: Hacer que se quede con los 4 que se hayan elegido y no los 4 primeros
         * 
         */
        foreach(var learnableMove in _base.LearnableMoves)
        {
            if (learnableMove.Level <= _level)
            {
                _moves.Add(new Move(learnableMove.Move));
            }

            if (_moves.Count >= PokemonBase.NUMBER_OF_LEARNABLES_MOVES)
            {
                break;
            }
        }
    }

    public LearnableMove GetLearnableMoveAtCurrentLevel()
    {
        return Base.LearnableMoves.Where(learnableMove => learnableMove.Level == _level).FirstOrDefault();
    }

    public void LearnMove(LearnableMove learnableMove)
    { 
        if (Moves.Count >= PokemonBase.NUMBER_OF_LEARNABLES_MOVES)
        {
            return;
        }

        Moves.Add(new Move(learnableMove.Move));
    }

    public DamageDescription ReceiveDamage(Pokemon attacker, Move move)
    {

        float critical = 1f;

        if(Random.Range(0, 100f)< 8f)
        {
            critical = 2f;
        }

        float type1 = TypeMatrix.GetMultEffectiveness(move.Base.Type, this.Base.Type1);
        float type2 = TypeMatrix.GetMultEffectiveness(move.Base.Type, this.Base.Type2);
        float types = type1 * type2;

        var damageDescription = new DamageDescription()
        {
            Critical = critical,
            Type = types,
            Fainted = false
        };

        float attack = (move.Base.IsSpecialMove ? attacker.SpAttack : attacker.Attack);
        float defense = (move.Base.IsSpecialMove ? this.SpDefense : this.Defense);

        float modifiers = Random.Range(0.85f, 1.0f) * types * critical;

        float baseDamage = (
                (2 * attacker.Level / 5f + 2) *
                move.Base.Power *
                ((float)attack / defense)
            ) / 50f + 2;
        int totalDamage = Mathf.FloorToInt(baseDamage * modifiers);

        HP -= totalDamage;


        if(HP <= 0)
        {
            HP = 0;
            damageDescription.Fainted = true;
        }

        return damageDescription;
    }

    public Move RandomMove()
    {
        List<Move> movesWithPP = Moves.Where(move => move.Pp > 0).ToList();
        if(movesWithPP.Count > 0)
        {
            int moveID = Random.Range(0, movesWithPP.Count);
            return movesWithPP[moveID];
        }
        //No hay PPs en ning�n ataque
        //TODO: implementar combate, que hace da�o al enemigo y a ti mismo
        return null;
    }

    public bool NeedsToLevelUp()
    {
        if(Experience > Base.GetNecessaryExpForLevel(_level+1))
        {
            int currentMaxHP = MaxHP;
            _level++;
            HP += (MaxHP - currentMaxHP);

            return true;
        }
        else
        {
            return false;
        }
    }

    public class DamageDescription
    {
        public float Critical { get; set; }
        public float Type { get; set; }
        public bool Fainted { get; set; }
    }
}
