using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PokemonParty : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] List<Pokemon> pokemons;
    public const int NUM_MAX_POKEMON_IN_PARTY = 6;

    public List<Pokemon> Pokemons
    {
        get => pokemons;
        set => pokemons = value;
    }

    private void Start()
    {
        foreach(Pokemon pokemon in pokemons)
        {
            pokemon.InitPokemon();
        }
    }

    public Pokemon GetFirstNonFaintedPokemon()
    {
        return pokemons.Where(pokemon => pokemon.HP > 0).FirstOrDefault();
    }

    public int GetPositionFromPokemon(Pokemon pokemon)
    {
        for (int i = 0; i < Pokemons.Count; i++)
        {
            if (pokemon == Pokemons[i])
            {
                return i;
            }
        }

        return -1;
    }

    public bool AddPokemonToParty (Pokemon pokemon)
    {
        if(pokemons.Count < NUM_MAX_POKEMON_IN_PARTY)
        {
            pokemons.Add(pokemon);
            return true;
        } else
        {
            return false;
            //TODO a�adir funcionalidad de enviar al PC de Bill
        }
    }
}
