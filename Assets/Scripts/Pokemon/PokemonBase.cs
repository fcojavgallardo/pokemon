using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pokemon", menuName ="Pokemon/New Pokemon")]
public class PokemonBase : ScriptableObject
{
    [SerializeField] private int ID;

    #region Name and description
    [SerializeField] 
    private string pokemonName;
    public string Name => pokemonName;
    
    [TextArea][SerializeField] private string description;
    public string Description => description;
    #endregion

    #region sprites
    [SerializeField] private Sprite frontSprite;
    [SerializeField] private Sprite backSprite;

    public Sprite FrontSprite => frontSprite;
    public Sprite BackSprite => backSprite;
    #endregion

    #region types
    [SerializeField] private PokemonType type1;
    public PokemonType Type1 => type1;
    [SerializeField] private PokemonType type2;
    public PokemonType Type2 => type2;
    #endregion

    #region stats
    [SerializeField] private int maxHP;
    [SerializeField] private int attack;
    [SerializeField] private int defense;
    [SerializeField] private int spAttack;
    [SerializeField] private int spDefense;
    [SerializeField] private int speed;
    [SerializeField] private int expBase;
    [SerializeField] private GrowthRate growthRate;

    [SerializeField] private int catchRate = 255;

    public int MaxHP => maxHP;
    public int Attack => attack;
    public int Defense => defense;
    public int SpAttack => spAttack;
    public int SpDefense => spDefense;
    public int Speed => speed;
    public int CatchRate => catchRate;
    public int ExpBase => expBase;
    public GrowthRate GrowthRate => growthRate;
    #endregion

    #region Lista de movimientos
    [SerializeField] private List<LearnableMove> learnableMoves;
    public List<LearnableMove> LearnableMoves => learnableMoves;
    public static int NUMBER_OF_LEARNABLES_MOVES { get; } = 4;
    #endregion

    public int GetNecessaryExpForLevel(int level)
    {
        switch(GrowthRate)
        {
            case GrowthRate.Fast:
                return Mathf.FloorToInt(4 * Mathf.Pow(level, 3) / 5);
            case GrowthRate.MediumFast:
                return Mathf.FloorToInt(Mathf.Pow(level, 3));
            case GrowthRate.MediumSlow:
                return Mathf.FloorToInt(6 * Mathf.Pow(level, 3) / 5 
                    - 15*Mathf.Pow(level, 2)+ 100*level - 140);
            case GrowthRate.Slow:
                return Mathf.FloorToInt(5 * Mathf.Pow(level, 3) / 4); ;
            default:
                return -1;
        }
    }
}

public enum GrowthRate
{
    Fast,
    MediumFast,
    MediumSlow,
    Slow
}

public enum PokemonType
{
    None,
    Normal,
    Fire,
    Water,
    Electric,
    Grass,
    Ice,
    Fight,
    Poison,
    Ground,
    Fly,
    Psychic,
    Bug,
    Rock,
    Ghost,
    Dragon,
    Dark,
    Steel,
    Fairy    
}

public class TypeColor
{
    static Color[] colors =
    {
        Color.white, // NONE
        new Color (0.2f, 0.2f, 0.2f), //TODO: Normal
        new Color (0.990566f, 0.5957404f, 0.5279903f), //Fire
        new Color (0.5613208f, 0.7828107f, 1f), // Water
        new Color (0.2f, 0.2f, 0.2f), //TODO: Electric
        new Color (0.4103774f, 1f, 0.6846618f), // Grass
        new Color (0.2f, 0.2f, 0.2f), //TODO: Ice
        new Color (0.2f, 0.2f, 0.2f), //TODO: Fight
        new Color (0.2f, 0.2f, 0.2f), //TODO: Poison
        new Color (0.2f, 0.2f, 0.2f), //TODO: Ground
        new Color (0.2f, 0.2f, 0.2f), //TODO: Fly
        new Color (0.2f, 0.2f, 0.2f), //TODO: Psychic
        new Color (0.8193042f, 0.9333333f, 0.5254902f), // Bug
        new Color (0.2f, 0.2f, 0.2f), //TODO: Rock
        new Color (0.2f, 0.2f, 0.2f), //TODO: Ghost
        new Color (0.2f, 0.2f, 0.2f), //TODO: Dragon
        new Color (0.2f, 0.2f, 0.2f), //TODO: Dark
        new Color (0.2f, 0.2f, 0.2f), //TODO: Steel
        new Color (0.2f, 0.2f, 0.2f), //TODO: Fairy
    };

    public static Color GetColorFromType(PokemonType type)
    {
        return colors[(int)type];
    }
}
public class TypeMatrix
{
    static float[][] matrix =
    {
        //                    NOR    FIR    WAT    ELE    GRA    ICE    FIG    POI    GRO    FLY    PSY    BUG    ROC    GHO    DRA    DAR    STE    FAI
        /*NOR*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    0.5f,  0f,    1f,    0.5f,  1f,    1f},
        /*FIR*/ new float [] {1f,    0.5f,  0.5f,  1f,    2f,    2f,    1f,    1f,    1f,    1f,    1f,    2f,    0.5f,  1f,    0.5f,  0.5f,  2f,    1f},
        /*WAT*/ new float [] {1f,    2f,    0.5f,  0.5f,  0.5f,  1f,    1f,    1f,    2f,    1f,    1f,    1f,    2f,    1f,    0.5f,  1f,    1f,    1f},
        /*ELE*/ new float [] {1f,    1f,    2f,    0.5f,  0.5f,  1f,    1f,    1f,    0f,    2f,    1f,    1f,    1f,    1f,    0.5f,  1f,    1f,    1f},
        /*GRA*/ new float [] {1f,    0.5f,  2f,    1f,    0.5f,  1f,    1f,    0.5f,  2f,    0.5f,  1f,    0.5f,  2f,    1f,    0.5f,  1f,    1f,    1f},
        /*ICE*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f},
        /*FIG*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f},
        /*POI*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f},
        /*GRO*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f},
        /*FLY*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f},
        /*PSY*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f},
        /*BUG*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f},
        /*ROC*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f},
        /*GHO*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f},
        /*DRA*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f},
        /*DAR*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f},
        /*STE*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f},
        /*FAI*/ new float [] {1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f,    1f}
    };

    public static float GetMultEffectiveness(PokemonType attackType, PokemonType defenderType)
    {
        if(attackType == PokemonType.None || defenderType == PokemonType.None)
        {
            return 1.0f;
        }

        int row = (int)attackType;
        int col = (int)defenderType;

        return matrix[row - 1][col - 1];
    }
}

[Serializable]
public class LearnableMove
{
    [SerializeField] private MoveBase _move;
    [SerializeField] private int _level;

    public MoveBase Move => _move;
    public int Level => _level;
}