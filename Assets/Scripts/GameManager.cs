using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    Travel,
    Battle
}
public class GameManager : MonoBehaviour
{
    [SerializeField] PlayerController playerController;
    [SerializeField] BattleManager battleManager;
    [SerializeField] Camera worldMainCamera;

    private GameState _gameState;

    public AudioClip worldClip;
    public AudioClip battleClip;

    void Awake()
    {
        _gameState = GameState.Travel;
    }

    void StartPokemonBattle()
    {
        SoundManager.SharedInstance.PlayMusic(battleClip);

        _gameState = GameState.Battle;
        battleManager.gameObject.SetActive(true);
        worldMainCamera.gameObject.SetActive(false);

        var playerParty = playerController.GetComponent<PokemonParty>();
        var wildPokemon = FindObjectOfType<PokemonMapArea>().GetComponent<PokemonMapArea>().GetRandomWildPokemon();

        var wildPokemonCopy = new Pokemon(wildPokemon.Base, wildPokemon.Level);

        battleManager.HandleStartBattle(playerParty, wildPokemonCopy);
    }

    void EndPokemonBattle(bool hasWon)
    {
        SoundManager.SharedInstance.PlayMusic(worldClip);
        _gameState = GameState.Travel;
        battleManager.gameObject.SetActive(false);
        worldMainCamera.gameObject.SetActive(true);
    }

    private void Start()
    {
        SoundManager.SharedInstance.PlayMusic(worldClip);
        playerController.OnPokemonEncountered += StartPokemonBattle;
        battleManager.OnBattleFinish += EndPokemonBattle;
    }

    void Update()
    {
        if(_gameState == GameState.Travel)
        {
            playerController.HandleUpdate();
        }
        else if(_gameState == GameState.Battle)
        {
            battleManager.HandleUpdate();
        }
    }
}


